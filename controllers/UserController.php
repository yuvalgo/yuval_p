<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\userSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{


   

    public function actionIndex()
    {
        return $this->gohome();
    }

    public function actionAdd()
    {
        $user = new User();
        $user ->username = 'admin';
        $user->password = 'admin';
        $user->save();
        $user = new User();
        $user ->username = 'user';
        $user->password = 'user';
        $user->save();
        $user = new User();
        $user ->username = 'yoyo';
        $user->password = 'yoyo';
        $user->save();
        $user = new User();
        $user ->username = 'bibi';
        $user->password = 'bibi';
        $user->save();
        return $this->gohome();
    }
}
