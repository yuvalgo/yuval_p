<?php

use yii\db\Migration;

class m160521_061000_init_user_table extends Migration
{
    public function up()
    {
    	$this->createTable(
    			'user',
    			[
    				'id' => 'pk',
    				'username' => 'string',
    				'password' => 'string',
    				'auth_key' => 'string',
    				'email' => 'string',
    				'phone' => 'string',
    				'created_at' =>'integer',
    				'updated_at' => 'integer',
    				'created_by' => 'integer',
    				'updated_by' => 'integer',

    				],

    				'ENGINE = InnoDB'
    		);
    }

    public function down()
    {
   			$this->dropTable('user');
      
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
