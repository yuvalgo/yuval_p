<?php

use yii\db\Migration;

class m160504_175121_students_create extends Migration
{
    public function up()
    {
        $this->createTable(
            'students',
            [
                'id' => 'pk',
				'name' => 'string',
            ],
            'ENGINE=InnoDB'
        );
    }
    public function down()
    {
        $this->dropTable('students');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
